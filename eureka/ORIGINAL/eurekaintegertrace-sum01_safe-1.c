int main() { 
  int i, n, sn;
  
  assume(n>=0 && n<1000);
  skip
  sn=0;
  i=1;
  while (i<=n) {
    sn = sn + 2;
    i=i+1;
  }
  assert((sn>n+n-2 && sn<=n+n) || sn == 0);
  skip
}
