/*  -*- Last-Edit:  Fri Jan 29 11:13:27 1993 by Tarak S. Goradia; -*- */
/* $Log: tcas.c,v $
 * Revision 1.2  1993/03/12  19:29:50  foster
 * Correct logic bug which didn't allow output of 2 - hf
 * */



int main()
{
  int CurVerticalSep;
  int HighConfidence;
  int TwoofThreeReportsValid;
  
  int OwnTrackedAlt;
  int OwnTrackedAltRate;
  int OtherTrackedAlt;
  
  int AltLayerValue;
  int PositiveRAAltThresh[4];
  
  int UpSeparation;
  int DownSeparation;
    
  int OtherRAC;
  int OtherCapability;
  int ClimbInhibit;
  
  int upwardpreferredclimb;
  int resultclimb;
  int upwardpreferreddescend;
  int resultdescend;
  
  int enabled, tcasequipped, intentnotknown;
  int needupwardRA, needdownwardRA;
  int altsep;
  

  PositiveRAAltThresh[0] = 400;
  PositiveRAAltThresh[1] = 500;
  PositiveRAAltThresh[2] = 640;
  PositiveRAAltThresh[3] = 740;


  assume(UpSeparation<PositiveRAAltThresh[0] && DownSeparation>=PositiveRAAltThresh[0]);
  skip
  assume(AltLayerValue>=0 && AltLayerValue<=3);
    skip
    if((HighConfidence<0) && (OwnTrackedAltRate <= 600) && (CurVerticalSep > 600)){
      enabled = 1;
    } else {
      enabled = 0;
    }
  skip
    if((HighConfidence>0) && (OwnTrackedAltRate <= 600) && (CurVerticalSep > 600)){
      enabled = 1;
    } else {
      enabled = 0;
    }
  skip
    if(OtherCapability != 0){
      tcasequipped = 1;
    } else {
      tcasequipped = 0;
    }
  skip
    if((TwoofThreeReportsValid<0) && OtherRAC == 0){
      intentnotknown = 1;
    } else {
      intentnotknown = 0;
    }
  skip
    if((TwoofThreeReportsValid>0) && OtherRAC == 0){
      intentnotknown = 1;
    } else {
      intentnotknown = 0;
    }
  altsep = 0;
  if ((enabled!=0) && ((tcasequipped!=0 && intentnotknown!=0) || tcasequipped==0)){
    /// Procedure Non_Crossing_Biased_Climb()
    if(ClimbInhibit!=0){
      skip
	if((UpSeparation + 100) > DownSeparation){
	  upwardpreferredclimb = 1;
	} else {
	  upwardpreferredclimb = 0;
	}
      skip
	} else {
      skip
	if(UpSeparation > DownSeparation){
	  upwardpreferredclimb = 1;
	} else {
	  upwardpreferredclimb = 0;
	}
      skip
	}
    skip
      if (upwardpreferredclimb!=0){
	if((OwnTrackedAlt >= OtherTrackedAlt) || ((OwnTrackedAlt < OtherTrackedAlt) && (DownSeparation < PositiveRAAltThresh[AltLayerValue]))){
	  resultclimb = 1;
	} else {
	  resultclimb = 0;
	}
      skip
	}
      else {
	if((OtherTrackedAlt < OwnTrackedAlt) && (CurVerticalSep >= 300) && (UpSeparation >= PositiveRAAltThresh[AltLayerValue])){
	  resultclimb = 1;
	} else {
	  resultclimb = 0;
	}
      skip
	}
    skip
      if((resultclimb!=0) && (OwnTrackedAlt < OtherTrackedAlt)){
	needupwardRA = 1;
      } else {
	needupwardRA = 0;
      }
    skip

      /// Procedure Non_Crossing_Biased_Descend()
      if(ClimbInhibit<0){
	if((UpSeparation + 100) > DownSeparation){      
	  upwardpreferreddescend = 1;
	} else {
	  upwardpreferreddescend = 0;
	}
      skip
	} else {
      skip
	if((UpSeparation) > DownSeparation){      
	  upwardpreferreddescend = 1;
	} else {
	  upwardpreferreddescend = 0;
	}
      skip
	}
    skip
      if(ClimbInhibit>0){
	if((UpSeparation + 100) > DownSeparation){      
	  upwardpreferreddescend = 1;
	} else {
	  upwardpreferreddescend = 0;
	}
      skip
	} else {
      skip
	if((UpSeparation) > DownSeparation){      
	  upwardpreferreddescend = 1;
	} else {
	  upwardpreferreddescend = 0;
	}
      skip
	}
    skip
      if (upwardpreferreddescend!=0){
	if((OwnTrackedAlt < OtherTrackedAlt) && (CurVerticalSep >= 300) && (DownSeparation >= PositiveRAAltThresh[AltLayerValue])){
	  resultdescend = 1;
	} else {
	  resultdescend = 0;
	}
      skip
	}
      else{
      skip
	if((OtherTrackedAlt >= OwnTrackedAlt) || ((OtherTrackedAlt < OwnTrackedAlt) && (UpSeparation >= PositiveRAAltThresh[AltLayerValue]))){
	  resultdescend = 1;
	} else {
	  resultdescend = 0;
	}
      skip
	}
    skip
      if((resultdescend!=0) && (OtherTrackedAlt < OwnTrackedAlt)){
	needdownwardRA = 1;
      } else {
	needdownwardRA = 0;
      }
    skip
      if ((needupwardRA!=0) && (needdownwardRA!=0)){
	/* unreachable: requires Own_Below_Threat and Own_Above_Threat
	    to both be true - that requires Own_Tracked_Alt < Other_Tracked_Alt
	    and Other_Tracked_Alt < Own_Tracked_Alt, which isn't possible */
	//assert(1==0);
	altsep = 0;
      } else {
      skip
	if (needupwardRA!=0){
	  altsep = 1;
	} else {
	  skip
	    if (needdownwardRA!=0){
	      altsep = 2;
	    } else {
	      altsep = 0;
	    }
	  skip
	    }
      skip
	}
    skip
      } else {skip}
  skip
    assert(altsep !=1);
  skip
    }
