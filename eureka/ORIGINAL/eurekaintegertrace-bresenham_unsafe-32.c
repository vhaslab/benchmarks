void main() {
  int x0, y0, x1, y1;
  int dy;
  int dx;
  int stepx, stepy;
  int fraction;

  x0=undef;
  y0=undef;
  x1=x0+32;
  y1=y0+32;
  dy = y1 - y0;
  dx = x1 - x0+1;
  if (dy < 0) { dy = -dy;  stepy = -1; } else { stepy = 1; }
  skip
  if (dx < 0) { dx = -dx;  stepx = -1; } else { stepx = 1; }
  skip
  dy = dy*2;
  dx = dx*2;

  if (dx > dy) {
    fraction = 2*dy - dx;
    while (x0 < x1 || x0 > x1) {
      skip
      if (fraction >= 0) {
	y0 = y0 + stepy;
	fraction = fraction - 2*dx;
      } else {skip}
      skip
      x0 = x0 + stepx;
      fraction = fraction + 2*dy;  
    }
    skip
  } else {
    fraction = 2*dx - dy;
    while (y0 < y1 || y0 > y1) {
      skip
      if (fraction >= 0) {
	x0 = x0 + stepx;
	fraction = fraction - 2*dy;
      } else {skip}
      skip
      y0 = y0 + stepy;
      fraction = fraction + 2*dx;
    }
    skip
  }
  skip
  assert(y1==y0);
  skip
}

