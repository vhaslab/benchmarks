int main() { 
  int i, n, sn;
  
  assume(n>=0 && n<1000);
  sn=0;
  i=1;
  while(i<=n) {
    sn = sn + 2;
    if (i==4) {sn=-10;} else {skip}
    i=i+1;
  }
  skip
  assert(sn==n*2 || sn == 0);
  skip
}
