void main() {
  int a[4];
  int i,j,temp;
  
  i = 0;
  while(i<4) {
    a[i] = undef;
      i = i+1;
  }
  j = 1;
  while(j<4){
    temp = a[j];
    i = j-1;
    while(i>=0 && a[i]<temp){
      a[i+1] = a[i];
      i = i-1;
    }
    a[i+1]=temp;
    j=j+1;
  }
 assert(a[0]<=a[0+1] && a[1]<=a[1+1] && a[2]<=a[2+1]);

 skip
}
