int ans;

int fib(int n){
 int  i, Fnew, Fold, temp;

 Fnew = 1;  
 Fold = 0;
 i = 2;
 while( i <= n ) {
  temp = Fnew;
  Fnew = Fnew + Fold;
  Fold = temp;
  i=i+1;
 }
 ans = Fnew;
 assert(i == n+1);
 skip
}
    
void main()
{
  int a;

  a = 100;
  fib(a);
  skip
  assert(ans==354224848179261915075);
  skip
}
