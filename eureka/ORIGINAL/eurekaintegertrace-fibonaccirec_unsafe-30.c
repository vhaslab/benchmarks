int ris;

void fib(int k)
{
  int tmp;

  if (k>2) 
    {
      ris=0; // comment this line to inhibit summary edge optimiztion
      fib(k-1);
      skip
      tmp = ris;
      ris=0; // comment this line to inhibit summary edge optimiztion
      fib(k-2);
      skip
      ris=tmp+ris;
    }
  else 
    {
      ris = k;
    }
  skip
}

void main()
{
  int n,tmp1;
  
  n=30;
  ris=0;
  fib(n-1);
  skip
  tmp1=ris;
  ris=0;
  fib(n-2);
  skip
  tmp1=tmp1+ris;
  assert(tmp1==832040);
  skip
}
