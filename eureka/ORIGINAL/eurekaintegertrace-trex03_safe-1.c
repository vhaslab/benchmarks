int main()
{
  int x1,x2,x3;
  int d1, d2, d3;
  int c1, c2;
  
  assume (x1==10 && x2==10 && x3==10);
  c1=undef;
  c2=undef;
  d1=1;
  d2=1;
  d3=1;
  while(x1>=1 && x2>=1 && x3>=1)
  {
    if (c1!=0) {x1=x1-d1;} 
    else {
      if (c2!=0) {x2=x2-d2;}
      else {x3=x3-d3;}
      skip
    }
    c1=undef;
    c2=undef;
  }
  skip
  assert(x1==0 || x2==0 || x3==0);
  skip
}

