main()
{
  char string_A[5], string_B[5];
  int i, j, nc_A, nc_B, found;
  

  found=0;
  i=0;
  while(i<5) {
    string_A[i]=undef;    
    i=i+1;
  }
  assume(string_A[5-1]==0);

  i=0;
  while(i<5) {
    string_B[i]= undef;    
    i=i+1;
  }
  assume(string_B[5-1]==0);

  nc_A = 0;
  while(nc_A<5 && string_A[nc_A]!=0) {
    nc_A=nc_A+1;
  }
  nc_B = 0;
  while(nc_B<5 && string_B[nc_B]!=0) {
    nc_B=nc_B+1;
  }
  assume(nc_B >= nc_A);
  
  i=0;
  j=0;
  while((i<nc_A) && (j<nc_B))
  {
    if(string_A[i] == string_B[j]) 
    {
       i=i+1;
       j=j+1;
    }   
    else
    {
       i = i-j+1;
       j = 0;
    }   
    skip
  }
  skip
    if (j > (nc_B-1)) { 
      found = 1;
    } else { found = 0;}
  found = found+found;
  assert(found == 0 || found == 1);
  skip
}

