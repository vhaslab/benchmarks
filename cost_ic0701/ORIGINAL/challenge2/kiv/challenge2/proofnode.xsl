<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml">

<!-- output method html is important here: the map/area doesn't work otherwise! -->
<xsl:output method="html" encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="SEQ"> 
     <xsl:call-template name="kivtext"/>
</xsl:template>

<xsl:template match="/PROOFNODE">
<html>	
<head>
<title>Step <xsl:value-of select="./NUM"/> (<xsl:value-of select="./attribute::proofname"/>)</title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="theoremheader"/>

<h1>Proof step <xsl:value-of select="./NUM"/> from proof <xsl:value-of select="./attribute::proofname"/>
<xsl:choose>
<xsl:when test="0 = string-length(./attribute::previous)"> (conclusion)</xsl:when>
<xsl:otherwise> (<a><xsl:attribute name="href"><xsl:value-of select="concat(./attribute::previous,'.xml')"/></xsl:attribute>previous step</a>)
</xsl:otherwise>
</xsl:choose>
</h1>

	<table border="2" width="100%">
	<tr>
	<td class="seq"> 
	        <xsl:apply-templates select="./SEQ"/>

        </td>
        </tr>
        </table>

<xsl:choose>
<!-- ====================================================== -->
     <xsl:when test="./TYPE = 'step'">
               <h2><xsl:value-of select="./INFO"/></h2>

	<xsl:choose>
        <xsl:when test="0 = string-length(./NEWGOALS)"><h2>And closed the goal!</h2></xsl:when>
        <xsl:otherwise>
               <h2>And obtained the following new goals</h2>
                   <ul>
                   <xsl:for-each select="./NEWGOALS/LIST/LE">
                   <li> <a><xsl:attribute name="href"><xsl:value-of select="concat(./PAIR/FST, '.xml')"/></xsl:attribute>Proof step <xsl:value-of select="./PAIR/FST"/></a>:
                        <p class="seq"><xsl:value-of select="./PAIR/SND/SEQ"/></p>
                  </li>
                   </xsl:for-each>
                  </ul>
         </xsl:otherwise>
         </xsl:choose>
               <h2>Rule argument</h2>
                   <xsl:value-of select="./RULEARGS"/>
               <h2>Additional information</h2>
                   <p class="seq"><xsl:value-of select="./EXTRAS"/></p>
    </xsl:when>
<!-- ====================================================== -->
     <xsl:when test="./TYPE = 'lemma'">
          <h2>Lemma <a><xsl:attribute name="href"><xsl:value-of select="concat('../',concat(./INFO,'/longlemmainfo.xml'))"/></xsl:attribute>
                   <xsl:value-of select="./INFO"/></a></h2>
    </xsl:when>
<!-- ====================================================== -->
     <xsl:when test="./TYPE = 'spec-lemma'">
          <h2>A spec-lemma</h2>
    </xsl:when>
<!-- ====================================================== -->
     <xsl:when test="./TYPE = 'open'">
          <h2>An open goal</h2>
    </xsl:when>
<!-- ====================================================== -->
     <xsl:when test="./TYPE = 'proof-lemma'">
          <h2>Proof step <a><xsl:attribute name="href"><xsl:value-of select="concat(./INFO,'.xml')"/></xsl:attribute><xsl:value-of select="./INFO"/></a> used as proof-lemma</h2>
    </xsl:when>
<!-- ====================================================== -->
     <xsl:when test="./TYPE = 'special'">
          <h2>Special step <xsl:value-of select="./INFO"/></h2>
	<xsl:choose>
        <xsl:when test="0 = string-length(./NEWGOALS)"><h2>And closed the goal!</h2></xsl:when>
        <xsl:otherwise>
               <h2>And obtained the following new goals</h2>
                   <ul>
                   <xsl:for-each select="./NEWGOALS/LIST/LE">
                   <li> <a><xsl:attribute name="href"><xsl:value-of select="concat(./PAIR/FST, '.xml')"/></xsl:attribute>Proof step <xsl:value-of select="./PAIR/FST"/></a>:
                        <p class="seq"><xsl:value-of select="./PAIR/SND/SEQ"/></p>
                  </li>
                   </xsl:for-each>
                  </ul>
         </xsl:otherwise>
         </xsl:choose>
    </xsl:when>
<!-- ====================================================== -->
     <xsl:otherwise>Something unknown</xsl:otherwise>
</xsl:choose>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
