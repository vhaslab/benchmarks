<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/SYMBOLTABLE">
<html> 
<head>
<title> Signature symbols in this unit </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="unitheader"/>

	<h1> Signature symbols in this unit </h1>
	<table border="1">
	<tr>
	<th class="tablehead"> Symbol </th>
	<th class="tablehead"> Specification </th>
	<th class="tablehead"> Kind </th>
	<th class="tablehead"> Type </th>
	</tr>

	<xsl:for-each select="./SIGENTRY">
        <tr><td>
                  <xsl:value-of select="./SYMBOL"/>
        </td><td>
                  <a><xsl:attribute name="href"><xsl:value-of select="./SPEC/HTMLLINK/HTMLTARGET"/>
                     </xsl:attribute>
                     <xsl:value-of select="./SPEC/HTMLLINK/HTMLSTRING"/>
                  </a>
        </td><td>
                  <xsl:value-of select="./TYPE"/>
        </td><td>
                  <xsl:value-of select="./ENTRY"/>
        </td></tr>
        </xsl:for-each>
        </table>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>

