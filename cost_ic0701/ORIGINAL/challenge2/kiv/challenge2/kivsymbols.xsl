<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/KIVSYMBOLS">
<html> 
<head>
<title> Mapping of KIV symbols </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="projectheader"/>

	<h1> Mapping of KIV symbols </h1>

        <p>The following table contains the special (mathematical) symbols used by KIV. You can check
        if your browser and your font supports all symbols. The second column should contain symbols
        that correspond to their symbolic name in the first column. If you see only a white box or
        a glyph that shows a hex-code this symbol is either not available in the currently selected
        font, or not supported by your browser. </p>

	<table border="2" class="cent">
	<tr>
	<th class="tablehead"> KIV Symbol name </th>
	<th class="tablehead"> UTF-8<br/>encoded<br/>character </th>
	<th class="tablehead"> Hex Unicode </th>
	<th class="tablehead"> Octal Code <br/>in KIV font </th>
	<th class="tablehead"> Ascii Code <br/>in KIV font </th>
	<th class="tablehead"> Hex Code <br/>in KIV font </th>
	<th class="tablehead"> UTF-8<br/>chars </th>
	</tr>
	<xsl:for-each select="./ENTRY">
	<tr>
            <td><xsl:value-of select="./NAME"></xsl:value-of></td>
            <td><xsl:value-of select="./UTF8"></xsl:value-of></td>
            <td><xsl:value-of select="./UNICODE"></xsl:value-of></td>
            <td><xsl:value-of select="./KFOCT"></xsl:value-of></td>
            <td><xsl:value-of select="./KFASCII"></xsl:value-of></td>
            <td><xsl:value-of select="./KFHEX"></xsl:value-of></td>
            <td><xsl:value-of select="./UTF8CHARS"></xsl:value-of></td>
        </tr>
	</xsl:for-each>
	</table>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>

