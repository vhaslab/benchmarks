<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="LEMMAGOAL/SEQGOAL/GOALSEQ/SEQ"> 
     <xsl:call-template name="kivtext"/>
</xsl:template>
<xsl:template match="LEMMAGOAL/JAVAGOAL/GOALTDS">
<xsl:for-each select="JKTYPEDECLARATIONS"> 
     <xsl:call-template name="kivtext"/>
</xsl:for-each>
</xsl:template>

<xsl:template match="/LONGLEMMAINFO">
<html> 
<head>
<title> Theorem <xsl:value-of select="./attribute::name"/> </title>

<xsl:call-template name="styledefs"/>

</head>
<body><xsl:call-template name="theoremheader"/>

	<h1> Theorem <xsl:value-of select="./attribute::name"/> </h1>

	<table border="2" width="100%">
	<tr>
	<td class="seq"> 
	        <xsl:apply-templates select="./LEMMAINFO/LEMMAGOAL"/>

        </td>
        </tr>
        </table>

        <ul>
             <xsl:for-each select="./LEMMAINFO/LEMMATYPE/AXIOMLEMMA"><li class="bgyellow">Type: An axiom</li></xsl:for-each>
             <xsl:for-each select="./LEMMAINFO/LEMMATYPE/JAVAAXIOMTYPE"><li class="bgyellow">Type: A Java axiom</li></xsl:for-each>
             <xsl:for-each select="./LEMMAINFO/LEMMATYPE/GENERATEDJAVAAXIOMTYPE"><li>Type: A generated Java axiom</li></xsl:for-each>
             <xsl:for-each select="./LEMMAINFO/LEMMATYPE/OBLIGATIONLEMMA"><li class="bgred">Type: A proof obligation</li></xsl:for-each>
             <xsl:for-each select="./LEMMAINFO/LEMMATYPE/USERLEMMA"><li class="bglightblue">Type: A user defined theorem</li></xsl:for-each>
	<li> Validity: <xsl:value-of select="./VALIDITY"/> </li>
	<li> Features: <xsl:for-each select="./FEATURES/LIST/LE"><xsl:value-of select="."/>, 
                      </xsl:for-each> </li>
<!--	<li> History:  <xsl:value-of select="./HISTORY"/> </li> -->
	<li> Comment:  <xsl:value-of select="./COMMENT"/> </li>

	<li> Used by:  <xsl:for-each select="./USEDBY/LIST/LE">
             <a><xsl:attribute name="href">
                <xsl:value-of select="concat('../',concat(.,'/longlemmainfo.xml'))"/>
               </xsl:attribute>
               <xsl:value-of select="."/></a>, 
             </xsl:for-each>
       </li>
	<li> Used by units in proved state: 
             <xsl:for-each select="./USEDBYUNITS/LIST/LE">
             <a><xsl:attribute name="href">
                             <xsl:value-of select="./HTMLLINK/HTMLTARGET"/>
               </xsl:attribute><xsl:value-of select="./HTMLLINK/HTMLSTRING"/>
            </a>, 
            </xsl:for-each>
       </li>

        <li> Proof exists?: <xsl:for-each select="./LEMMAINFO/PROOFEXISTS/T">Yes</xsl:for-each>
                            <xsl:for-each select="./LEMMAINFO/PROOFEXISTS/F">No</xsl:for-each>
       </li>
</ul>
<!-- ******************************************************************************************** -->
<!-- <xsl:for-each select="./LEMMAINFO/PROOFEXISTS/T"> -->
<xsl:if test="local-name(./LEMMAINFO/PROOFEXISTS/T) = 'T'">
<h2>View <a href="proof.xml">the proof</a></h2>

<h2>Infos about the proof</h2>

<ul>
             <xsl:for-each select="./LEMMAINFO/PROVED/T"><li class="bggreen"> Proved?: Yes</li></xsl:for-each>
             <xsl:for-each select="./LEMMAINFO/PROVED/F"><li>Proved?: No</li></xsl:for-each>
        <li> Interactions: <xsl:value-of select="./LEMMAINFO/USERACTIONS"/> </li>
        <li> Proof steps: <xsl:value-of select="./LEMMAINFO/PROOFSTEPS"/> </li>

        <li> Used lemmas: 
             <xsl:for-each select="./LEMMAINFO/USEDLEMMAS/LIST/LE"> 
             <a><xsl:attribute name="href">
                <xsl:value-of select="concat('../',concat(.,'/longlemmainfo.xml'))"/>
               </xsl:attribute>
               <xsl:value-of select="."/></a>, 
             </xsl:for-each>
       </li>

	<li> Used spec theorems:
             <ul>
             <xsl:for-each select="./USED_SPECLEMS/USED_SEQ">
             <li><a><xsl:attribute name="href">
                             <xsl:value-of select="./SPECNAME/HTMLLINK/HTMLTARGET"/>
                          </xsl:attribute><xsl:value-of select="./SPECNAME/HTMLLINK/HTMLSTRING"/>
            </a>/<xsl:if test="0 != string-length(./INSTNAME)"><xsl:value-of select="./INSTNAME"/>/</xsl:if><a><xsl:attribute name="href">
          <xsl:value-of select="./LEMNAME/HTMLLINK/HTMLTARGET"/>
         </xsl:attribute><xsl:value-of select="./LEMNAME/HTMLLINK/HTMLSTRING"/></a>
         </li>
            </xsl:for-each>
            </ul>
       </li>

	<li> Used simplifier rules:
             <ul>
             <xsl:for-each select="./USED_SIMPRULES/USED_SEQ">
             <li>
             <a><xsl:attribute name="href">
                <xsl:value-of select="./SPECNAME/HTMLLINK/HTMLTARGET"/>
               </xsl:attribute><xsl:value-of select="./SPECNAME/HTMLLINK/HTMLSTRING"/>
            </a>/<xsl:if test="0 != string-length(./INSTNAME)"><xsl:value-of select="./INSTNAME"/>/</xsl:if><a><xsl:attribute name="href">
          <xsl:value-of select="./LEMNAME/HTMLLINK/HTMLTARGET"/>
         </xsl:attribute><xsl:value-of select="./LEMNAME/HTMLLINK/HTMLSTRING"/></a>
          </li>
            </xsl:for-each>
          </ul>
       </li>
</ul>

</xsl:if>
</body>
</html> 
</xsl:template>
</xsl:stylesheet>
