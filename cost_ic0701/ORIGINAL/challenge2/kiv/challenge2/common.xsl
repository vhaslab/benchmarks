<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<!-- <xsl:output  indent="yes" encoding="UTF-8"/> -->
<!-- <xsl:output method="html"/> -->
<!-- <xsl:output method="html" encoding="UTF-8"/> -->

<xsl:template name="styledefs">
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<style type="text/css">
        .cent {text-indent:12px}
	.bgyellow {background-color:#FFFF00}
	.bgblue {background-color:#ade4FF}
        .bglightblue {background-color:#8deeee}
	.bggreen {background-color:#00FF00}
	.bgred {background-color:#FF0000}
	.bgorange {background-color:#ffa500}
	.bggrey {background-color:#efefde}
	.tableheadsmall {background-color:#ade4FF; font-size:50%; text-indent:0px }
	.tablehead {background-color:#ade4FF; font-size:100%; text-indent:0px }
	.tableheadbig {background-color:#ade4FF; font-size:200% }
	.specheader {background-color:#FFFF00; font-size:200% ; text-align: left}
	.uninst     {color:red; font-size:200%}
	.seq { white-space:pre; font-family:monospace ; font-size:150%  }
	.lemmanamelink { vertical-align:top; font-size:150% ; text-align: left}
	.axiomname {font-family:monospace; vertical-align:top; color:#00FF0F; font-size:150% ; text-align: left}
	.usedspecs { font-size:150% ; text-align: left}
	.comment { font-size:100% ; text-align: left}
	.linkedspec { font-size:150% ; text-align: left}
	</style>
</xsl:template>

<xsl:template name="projectheader">
        Back to the <a href="index.xml">index</a>.
        View the <a href="project.xml">project with graph</a>. 
        View the <a href="projectwithoutgraph.xml">project without graph</a>. 
        View the <a href="devgraph.png">development graph</a> on its own page. 
        View <a href="statistic.xml">project statistic</a>. 
        View <a href="projectsymbols.xml">project symbols</a>. 
        View <a href="kivsymbols.xml">KIV symbols</a> to check that all symbols are available.
</xsl:template>

<xsl:template name="unitheader">
	Back to <a href="../../../index.xml">main index</a>. 
	Back to <a href="../../../project.xml">project overview</a>. 
        View <a href="unit.xml">the unit</a>.
        View <a href="lemmasummary.xml">theorem base summary</a>,
        View <a href="lemmabase.xml">theorem base listing</a>,
<!--        the <a href="usedproperties.xml">used properties</a>, -->
        the <a href="usedpropertiesbyspec.xml">used properties by spec</a>, 
        the <a href="dependency.xml">dependency list</a>, 
        the <a href="symboltable.xml">symbol table</a>, 
</xsl:template>

<xsl:template name="theoremheader">
        View <a href="longlemmainfo.xml">long theorem info</a>.
        View <a href="proof.xml">the proof</a>.
	Back to <a href="../../../../index.xml">main index</a>. 
	Back to <a href="../../../../project.xml">project overview</a>. 
        View <a href="../unit.xml">the unit</a>.
        View <a href="../lemmasummary.xml">theorem base summary</a>,
        View <a href="../lemmabase.xml">theorem base listing</a>,
<!--        the <a href="../usedproperties.xml">used properties</a>, -->
        the <a href="../usedpropertiesbyspec.xml">used properties by spec</a>, 
        the <a href="../dependency.xml">dependency list</a>, 
        the <a href="../symboltable.xml">symbol table</a>, 
</xsl:template>

<xsl:template name="kivtext">
        <xsl:for-each select="text()|*">
	<xsl:choose>
	<xsl:when test="local-name(.) = 'a'"><a><xsl:attribute name="href"><xsl:value-of select="./attribute::href"/></xsl:attribute><xsl:value-of select="."/></a></xsl:when>
	<xsl:when test="local-name(.) = 'font'"><font><xsl:attribute name="color"><xsl:value-of select="./attribute::color"/></xsl:attribute><xsl:value-of select="."/></font></xsl:when>
	<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
