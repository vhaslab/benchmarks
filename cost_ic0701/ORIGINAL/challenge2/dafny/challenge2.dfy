// Start: 15:10
// Finish: 16:11

/*

COST Verification Competition
Please send solutions to vladimir@cost-ic0701.org

Challenge 2: Maximum in a tree


Given: A non-empty binary tree, where every node carries an integer.

Implement and verify a program that computes the maximum of the values
in the tree.

Please base your program on the following data structure signature:

public class Tree {

    int value;
    Tree left;
    Tree right;

}

You may represent empty trees as null references or as you consider
appropriate.

*/

class Tree {
	var value: int;
	var left: Tree;
	var right: Tree;
	
	ghost var repr: set<object>;
	
	function valid(): bool
		reads this, repr;
		decreases repr;
	{
		this in repr && 
		(left != null ==> left in repr && left.repr < repr && left.valid()) && 
		(right != null ==> right in repr && right.repr < repr && right.valid())
	}
	
	function method max(): int
		requires valid();
		reads repr;
		decreases repr;
		ensures max() >= value;
		ensures left != null ==> max() >= left.max();
		ensures right != null ==> max() >= right.max();
	{
		if right != null && right.max() > (if left != null && left.max() > value then left.max() else value) 
		then right.max() 
		else (if left != null && left.max() > value then left.max() else value)
	
	}
}

