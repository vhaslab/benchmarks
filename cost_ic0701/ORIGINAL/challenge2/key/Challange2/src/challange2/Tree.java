/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package challange2;

/**
 *
 * @author christoph
 */
public class Tree {
    int value;
    Tree /*@ nullable @*/ left;
    Tree /*@ nullable @*/ right;

    /*@ public model \seq seq;
        represents seq = \seq_concat(\seq_singleton(this.value), \seq_concat(left.seq, right.seq));
        accessible seq : footprint \measured_by seq.length;

        public model \locset footprint;
        represents footprint = this.*, left.footprint, right.footprint;
        accessible footprint : \nothing;
    @*/


    /*@ normal_behavior
//            ensures     \result >= value;
//            ensures     left != null ==> \result >= left.max();
//            ensures     right != null ==> \result >= right.max();
            ensures     true;
            accessible  footprint;
            measured_by  seq.length;
      @*/
    public /*@ pure @*/ int max() {
        int maxLeft = (left != null) ? left.max() : value;
        int maxRight = (right != null) ? right.max() : value;
        int maxChildern = (maxLeft <= maxRight) ? maxRight : maxLeft;
        return (value <= maxChildern) ? maxChildern : value;
    }

}
