(* This file is generated by Why3's Coq driver *)
(* Beware! Only edit allowed sections below    *)
Require Import ZArith.
Require Import Rbase.
Definition unit  := unit.

Parameter qtmark : Type.

Parameter at1: forall (a:Type), a -> qtmark -> a.

Implicit Arguments at1.

Parameter old: forall (a:Type), a -> a.

Implicit Arguments old.

Axiom Max_is_ge : forall (x:Z) (y:Z), (x <= (Zmax x y))%Z /\
  (y <= (Zmax x y))%Z.

Axiom Max_is_some : forall (x:Z) (y:Z), ((Zmax x y) = x) \/ ((Zmax x y) = y).

Axiom Min_is_le : forall (x:Z) (y:Z), ((Zmin x y) <= x)%Z /\
  ((Zmin x y) <= y)%Z.

Axiom Min_is_some : forall (x:Z) (y:Z), ((Zmin x y) = x) \/ ((Zmin x y) = y).

Axiom Max_x : forall (x:Z) (y:Z), (y <= x)%Z -> ((Zmax x y) = x).

Axiom Max_y : forall (x:Z) (y:Z), (x <= y)%Z -> ((Zmax x y) = y).

Axiom Min_x : forall (x:Z) (y:Z), (x <= y)%Z -> ((Zmin x y) = x).

Axiom Min_y : forall (x:Z) (y:Z), (y <= x)%Z -> ((Zmin x y) = y).

Axiom Max_sym : forall (x:Z) (y:Z), (y <= x)%Z -> ((Zmax x y) = (Zmax y x)).

Axiom Min_sym : forall (x:Z) (y:Z), (y <= x)%Z -> ((Zmin x y) = (Zmin y x)).

Inductive tree  :=
  | Null : tree 
  | Tree : Z -> tree -> tree -> tree .

Set Implicit Arguments.
Fixpoint mem(v:Z) (t:tree) {struct t}: Prop :=
  match t with
  | Null => False
  | (Tree x l r) => (x = v) /\ ((mem v l) /\ (mem v r))
  end.
Unset Implicit Arguments.

Set Implicit Arguments.
Fixpoint ge_tree(v:Z) (t:tree) {struct t}: Prop :=
  match t with
  | Null => True
  | (Tree x l r) => (x <= v)%Z /\ ((ge_tree v l) /\ (ge_tree v r))
  end.
Unset Implicit Arguments.

Axiom ge_trans : forall (x:Z) (y:Z) (t:tree), ((y <= x)%Z /\ (ge_tree y
  t)) -> (ge_tree x t).

(* YOU MAY EDIT THE CONTEXT BELOW *)

(* DO NOT EDIT BELOW *)

Theorem WP_parameter_max_aux : forall (t:tree), forall (acc:Z),
  match t with
  | Null => (ge_tree acc t) /\ (acc <= acc)%Z
  | (Tree v l r) => forall (result:Z), ((ge_tree result r) /\
      ((Zmax v acc) <= result)%Z) -> forall (result1:Z), ((ge_tree result1
      l) /\ (result <= result1)%Z) -> ((ge_tree result1 t) /\
      (acc <= result1)%Z)
  end.
(* YOU MAY EDIT THE PROOF BELOW *)
destruct t.
intro; split; simpl; auto with zarith.
intros acc r1 (H1,I1) r2 (H2,I2).
split.
simpl.
split.
apply

Qed.
(* DO NOT EDIT BELOW *)


