public class Tree {
  final int value;
  final Tree left;
  final Tree right;

  Tree(int value, Tree left, Tree right) {
    this.value = value;
    this.left = left;
    this.right = right;
  }

  /*
  int max() {
    int r = value;
    if (left != null && left.max() > r) r = left.max();
    if (right != null && right.max() > r) r = right.max();
    return r;
  }
  */
}

