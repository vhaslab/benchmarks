> | r2=@parameter2: * r1=@parameter1: * i0=@parameter0: * r0=@this:
  * true()=numeric_const("1") * zero()=numeric_const("0") * zero()=false()
  * TreeOrNull(@parameter1:, v_27) * TreeOrNull(@parameter2:, v_28)
  * field(@this:, "<Tree: Tree right>", nil())
  * field(r0, "<Tree: int value>", i0) * field(r0, "<Tree: Tree left>", r1)
 |- field(r0, "<Tree: Tree right>", _v_520) -| 

Match rule field_remove1
->field(r0, "<Tree: Tree right>", nil()) | r2=@parameter2: * r1=@parameter1:
   * i0=@parameter0: * r0=@this: * true()=numeric_const("1")
   * zero()=numeric_const("0") * zero()=false()
   * TreeOrNull(@parameter1:, v_27) * TreeOrNull(@parameter2:, v_28)
   * field(r0, "<Tree: int value>", i0) * field(r0, "<Tree: Tree left>", r1)
  |-  -| 

