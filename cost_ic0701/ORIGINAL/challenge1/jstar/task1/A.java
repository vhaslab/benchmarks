public class A {
  int maximum(int[] a, int n) {
    int x = 0;
    int y = n - 1; //a.length - 1;
    while (x != y) {
      if (a[x] <= a[y])
        ++x;
      else
        --y;
    }
    return x;
  }
}
