<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/USED_SIMPRULES_BY_SPEC">
<html> 
<head>
<title> Used simplifier rules/spec theorems by specification </title>

<xsl:call-template name="styledefs"/>

</head>
<body><xsl:call-template name="unitheader"/>

	<h1> Used simplifier rules/spec theorems by specification </h1>

	<xsl:for-each select="./SPECUSEDBY">
	<h2> Used from specification <a><xsl:attribute name="href">
                <xsl:value-of select="./SPECNAME/HTMLLINK/HTMLTARGET"/>
               </xsl:attribute><xsl:value-of select="./SPECNAME/HTMLLINK/HTMLSTRING"/></a>
       </h2>
             <ul><xsl:for-each select="./USED_IN">

             <li>
             <a><xsl:attribute name="href">
                <xsl:value-of select="./USED_SEQ/SPECNAME/HTMLLINK/HTMLTARGET"/>
               </xsl:attribute><xsl:value-of select="./USED_SEQ/SPECNAME/HTMLLINK/HTMLSTRING"/>
            </a>/<xsl:if test="0 != string-length(./USED_SEQ/INSTNAME)"><xsl:value-of select="./USED_SEQ/INSTNAME"/>/</xsl:if><a><xsl:attribute name="href">
          <xsl:value-of select="./USED_SEQ/LEMNAME/HTMLLINK/HTMLTARGET"/>
         </xsl:attribute><xsl:value-of select="./USED_SEQ/LEMNAME/HTMLLINK/HTMLSTRING"/></a> 
                 is used by
                             <xsl:for-each select="./LIST/LE">
                             <a><xsl:attribute name="href"><xsl:value-of select="concat(.,'/longlemmainfo.xml')"/></xsl:attribute><xsl:value-of select="."/></a>, 
                             </xsl:for-each>
             </li>
                  </xsl:for-each>
             </ul>
	</xsl:for-each>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>

