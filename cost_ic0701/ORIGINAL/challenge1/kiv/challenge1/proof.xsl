<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml">

<!-- output method html is important here: the map/area doesn't work otherwise! -->
<xsl:output method="html" encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/COORDINATES">
<html>	
<head>
<title> Proof for <xsl:value-of select="./NAME"/></title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="theoremheader"/>

	<h2> Proof Tree for	<xsl:value-of select="./NAME"/>
        (View <a><xsl:attribute name="href"><xsl:value-of select="concat(./CONCLUSION,'.xml')"/>
                   </xsl:attribute>conclusion</a>)
        </h2>

<p>The following image shows the proof tree. 
You can click on the nodes and the sequent and additional
rules will be displayed in a new window. 
</p>

	<map name="proofmap"> 
	<xsl:for-each select="./LIST/NODE">
		<area shape="rect" target="blank">
			<xsl:attribute name="coords"> 	
				<xsl:value-of select="./X"/>,
				<xsl:value-of select="./Y"/>,
				<xsl:value-of select="./X + 20"/>,
				<xsl:value-of select="./Y + 20"/>
			</xsl:attribute>
			<xsl:attribute name="href"> 	
				<xsl:value-of select="concat(./NUM,'.xml')"/>
			</xsl:attribute>
		</area>
	</xsl:for-each>
	</map> 
	<img usemap="#proofmap" src="./proof-tree.png">a proof tree</img>
	</body>
	</html>

</xsl:template>
</xsl:stylesheet>
