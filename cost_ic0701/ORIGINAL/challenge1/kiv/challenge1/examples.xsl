<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 
<xsl:output method="html"/>

<!-- ******************************************************************************************** -->
<!-- this is a named template, called with <xsl:call-template name="com"/>                        -->
<!-- It prints a COMMENT tag if it contains text                                                  -->
<xsl:template name="ifcom">
     <xsl:if test="0 != string-length(./COMMENT)">### Comment: <xsl:value-of select="./COMMENT"/>###</xsl:if>
</xsl:template>
<!-- ******************************************************************************************** -->
<!-- template to process a body of text that may contain A HREF's and FONT COLOR's                -->
<xsl:template match="BODY">
        <xsl:for-each select="text()|*">
	<xsl:choose>
	<xsl:when test="local-name(.) = 'a'"><a><xsl:attribute name="href"><xsl:value-of select="./attribute::href"/></xsl:attribute><xsl:value-of select="."/></a></xsl:when>
	<xsl:when test="local-name(.) = 'font'"><font><xsl:attribute name="color"><xsl:value-of select="./attribute::color"/></xsl:attribute><xsl:value-of select="."/></font></xsl:when>
	<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>
</xsl:template>
<!-- ******************************************************************************************** -->
<xsl:template match="PRETABLE">
<table border="1" width="100%">
<tr>
<th class="tablehead"> Name: </th>
<th class="tablehead"> Sequent: </th>
</tr>
<xsl:for-each select="./THEO">
<tr>
    <xsl:for-each select="./AXIOM">
    <xsl:attribute name="class"> bggreen </xsl:attribute>
    </xsl:for-each>
    <xsl:for-each select="./LEMMA">
    <xsl:attribute name="class"> bgblue </xsl:attribute>
    </xsl:for-each>
<td><xsl:value-of select="./NAME"/></td>
<td class="seq"><xsl:value-of select="./ENTRY"/></td>
</tr>
</xsl:for-each>
</table>
</xsl:template>
<!-- ******************************************************************************************** -->
<!-- ******************************************************************************************** -->
<!-- ******************************************************************************************** -->
<xsl:template match="/EXAMPLES">
<html>	
<head>
<title> Some examples by <xsl:value-of select="./attribute::NAME"/> </title>
<style type="text/css">
        .seq       {white-space:pre; font-family:monospace ; font-size:150%}
	.bgblue    {background-color:#ade4FF}
	.bggreen   {background-color:#00FF00}
	.tablehead {font-family:monospace; color:red; font-size:200% ; text-align: left}
	.yheader   {background-color:#FFFF00; font-size:200% ; text-align: left}
</style>
</head>
<body>
<h1>Some examples by <xsl:value-of select="./attribute::NAME"/></h1>
<!-- ******************************************************************************************** -->
<h2>These examples show use of if's: text is printed only when a tag contains text.</h2>
<xsl:for-each select="./IFEXAMPLE">
<h3 class="yheader"> Example number <xsl:value-of select="position()"/><xsl:if test="not(position()=last())"> (not the last one)</xsl:if></h3>
<xsl:call-template name="ifcom"/>
</xsl:for-each>
<!-- ******************************************************************************************** -->
<h2>Example: Here we copy A HREF's and FONT COLOR's together with the normal text</h2>
<xsl:apply-templates select="./BODY"/>
<!-- ******************************************************************************************** -->
<h2>Example: Use of css an Pre's in tables</h2>
<xsl:apply-templates select="./PRETABLE"/>
<!-- ******************************************************************************************** -->
</body>
</html>
</xsl:template>
<!-- ******************************************************************************************** -->
</xsl:stylesheet>
