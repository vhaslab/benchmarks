<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/LEMMABASE">
<html> 
<head>
<title> Theorem base for <xsl:value-of select="./attribute::name"/> </title>

<xsl:call-template name="styledefs"/>

</head>
<body><xsl:call-template name="unitheader"/>

	<h1> Theorem base for <xsl:value-of select="./attribute::name"/> </h1>

	<table border="1" width="100%">
	<tr>
	<th class="tableheadbig"> Name: </th>
	<th class="tableheadbig"> Sequent: </th>
	<th class="tableheadbig"> Type: </th>
	</tr>

	<xsl:for-each select="./THELEMMAS/LIST/LE/LEMMAINFO">
	<tr>
		<xsl:for-each select="./PROVED/T">
		<xsl:attribute name="class"> bggreen </xsl:attribute>
		</xsl:for-each>
		<xsl:for-each select="./PROVED/F">
		<xsl:attribute name="class"> bgblue </xsl:attribute>
		</xsl:for-each>

		<xsl:for-each select="./LEMMATYPE/AXIOMLEMMA">
		<xsl:attribute name="class"> bgyellow </xsl:attribute>
		</xsl:for-each>
		<xsl:for-each select="./LEMMATYPE/JAVAAXIOMTYPE">
		<xsl:attribute name="class"> bggrey </xsl:attribute>
		</xsl:for-each>
		<xsl:for-each select="./LEMMATYPE/GENERATEDJAVAAXIOMTYPE">
		<xsl:attribute name="class"> bggrey </xsl:attribute>
		</xsl:for-each>
		<xsl:for-each select="./LEMMATYPE/OBLIGATIONLEMMA">
		<xsl:attribute name="class"> bgyellow </xsl:attribute>
		</xsl:for-each>

	<td class="lemmanamelink">  <a >
			<xsl:attribute name="href"><xsl:value-of select="concat(./LEMMANAME,'/longlemmainfo.xml')"/></xsl:attribute> 
		  		<xsl:value-of select="./LEMMANAME"/> </a> </td>
	<td class="seq"> 
	        <xsl:value-of select="./LEMMAGOAL/SEQGOAL/GOALSEQ/SEQ"/> 
		<xsl:for-each select="./LEMMAGOAL/JAVAGOAL/GOALTDS/JKTYPEDECLARATIONS">
 			  <xsl:for-each select="./child::text() | ./font">
				<font><xsl:attribute name="color"> 
					<xsl:value-of select="./attribute::color"/>
				      </xsl:attribute>
					<xsl:value-of select="."/>
				</font>
			  </xsl:for-each>
		</xsl:for-each>
		</td>
	<td> 
		<xsl:for-each select="./LEMMATYPE/AXIOMLEMMA"> <xsl:text> An Axiom </xsl:text> </xsl:for-each> 
		<xsl:for-each select="./LEMMATYPE/USERLEMMA"> <xsl:text> A User Theorem </xsl:text> </xsl:for-each>
		<xsl:for-each select="./LEMMATYPE/JAVAXIOMTYPE"> <xsl:text> A Java Axiom </xsl:text> </xsl:for-each>
		<xsl:for-each select="./LEMMATYPE/GENERATEDJAVAAXIOMTYPE"> <xsl:text> A Generated Java Axiom </xsl:text> </xsl:for-each>
		<xsl:for-each select="./LEMMATYPE/OBLIGATIONLEMMA"> <xsl:text> A Proof Obligation </xsl:text> </xsl:for-each>
	</td>
	</tr>
	</xsl:for-each>
	</table>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>
