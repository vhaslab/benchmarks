/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package challange1;

/**
 *
 * @author christoph
 */
public class Max {

    /*@ normal_behavior
            requires    0 < a.length;
            requires    a.length < 2147483648;
            ensures     \result >= 0;
            ensures     \result < a.length;
            ensures     (\forall int i;
                         0 <= i && i < a.length;
                         a[i] <= a[\result]);
            assignable  \nothing;
      @*/
    public static int max(int[] a) {
        int x = 0;
        int y = a.length-1;

        /*@ loop_invariant
                0 <= x && x <= y && y < a.length
                && (\forall int i;
                    0 <= i && i <= x;
                    a[i] <= a[x] || a[i] <= a[y])
                && (\forall int j;
                    y < j && j < a.length;
                    a[j] <= a[x] || a[j] <= a[y]);
            assignable  \nothing;
            decreases   y - x;
          @*/
        while (x != y) {
            if (a[x] <= a[y]) x++;
                else y--;
        }
        return x;
    }

}
