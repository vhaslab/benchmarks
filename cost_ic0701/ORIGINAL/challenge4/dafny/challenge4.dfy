/*
COST Verification Competition
Please send solutions to vladimir@cost-ic0701.org

Challenge 4: Cyclic list

Given: A Java linked data structure with the signature:


public class Node {

    Node next;
    
    public boolean cyclic() {
        //...
    }

}

Implement and verify the method cyclic() to return true when the data
structure is cyclic (i.e., this Node can be reached by following next
links) and false when it is not.
*/

class Node {
	var next: Node;
	
	ghost var s: seq<Node>;  // Sequence of nodes reachable from 'this'
	ghost var repr: set<Node>; // Set of nodes reachable from 'this'
	
	static method SingletonAcyclic() returns (n: Node)
		ensures n != null && n.valid();
	{
		n := new Node;
		n.next := null;
		n.s := [n];
		n.repr := {n};
	}
	
	static method SingletonCyclic() returns (n: Node)
		ensures n != null && n.valid();
	{
		n := new Node;
		n.next := n;
		n.s := [n];
		n.repr := {n};
	}
	
	function is_cyclic(): bool
		requires valid();
		reads repr;
	{
		s[|s| - 1].next != null
	}
	
	method Cyclic() returns (res: bool)
		requires valid();
		ensures res == is_cyclic();
	{
		var n := this;
		ghost var i := 0;
		while (n.next != null && n.next != this)
			invariant n != null;
			invariant 0 <= i < |s|;
			invariant n == s[i];
			decreases |s| - i;
		{
			n := n.next;
			i := i + 1;
		}
		assert i == |s| - 1;
		res := n.next != null;
	}
		
	function valid(): bool
		reads this, repr;
	{
		(forall n :: n in s <==> n in repr) &&
		|s| > 0 && 
		s[0] == this && 
		(forall i :: 0 <= i < |s| ==> s[i] != null) && 
		(forall n :: n in s ==>
			n.repr <= repr &&
			(forall n1 :: n1 in n.s <==> n1 in n.repr) &&
			|n.s| > 0 && 
			n.s[0] == n && 
			(forall i :: 0 <= i < |n.s| ==> n.s[i] != null) && 
			(forall i :: 1 <= i < |n.s| ==> n.s[i] != n) && 
			(forall i :: 0 <= i < |n.s| - 1 ==> n.s[i].next == n.s[i + 1]) && 
			(n.s[|n.s| - 1].next == n || n.s[|n.s| - 1].next == null)
		)
	}
	
}

