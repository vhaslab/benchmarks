challenge3
-----------

The challenge is solved in two steps using the algorithms
defined in specification finddups.
The algorithms are both defined over arrays
with generic elements 
(an actualization with integers could be done).

The solution calls two very similar simple algorithms 
and is not very efficient (O(n^2)).

FINDDUPS# finds the first duplicate using two nested while loops,
which compute the two indices, where the duplicate can be found.

FINDDUPSSND# finds a duplicate where the value is not equal to
some given value "a". It is called with "a" = the first array element found.

Verification of both algorithms (theorems challenge3a and challenge3b) 
is very similar.

The main theorem challenge3 calls the two algorithms sequentially
and just composes the results.



Time started: 16.40
Time finished: 18.43