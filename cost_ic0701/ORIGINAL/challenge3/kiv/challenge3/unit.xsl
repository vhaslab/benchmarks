<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<!-- ******************************************************************************************** -->
<!-- template to process a specification body that may contain A HREF's and FONT COLOR's          -->
<xsl:template match="SPECBODY">
<xsl:call-template name="kivtext"/>
</xsl:template>
<!-- ******************************************************************************************** -->

<xsl:template match="/KIVSPEC">
<html>	
<head>
<title> Specification <xsl:value-of select="./attribute::name"/> </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="unitheader"/>

<p/>
	<div class="specheader"> <center>  Specification <xsl:value-of select="./attribute::name"/> </center> </div> 
	<xsl:for-each select="./attribute::uninstalled"><p class="uninst">The specification is not installed, i.e. it has no content (yet).</p></xsl:for-each>
	<pre>
	<xsl:apply-templates select="./SPECBODY"/>
	</pre>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
