<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 
<xsl:output  indent="yes" encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/LEMMADEPENDENCY">
<html> 
<head>
<title> Lemma dependencies for <xsl:value-of select="./attribute::name"/> </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="unitheader"/>

	<h1> Lemma dependency for <xsl:value-of select="./attribute::name"/> </h1>

        <p>A lemma (entry) may depend on lemmas (entries) below its position, but never on lemmas
        above its position. This means the list is a topological order of the dependency graph.
        An entry 'all subs proved' means that all used lemmas are (transitively) proved.</p>

	<table border="1">
	<tr>
	<th class="tablehead"> Name: </th>
	<th class="tablehead"> Status: </th>
	<th class="tablehead"> Subs proved? </th>
	<th class="tablehead"> Used: </th>
	</tr>

	<xsl:for-each select="./LE/DEPENTRY">
	<tr>
                <xsl:for-each select="./SUBSPROVED/T"><xsl:attribute name="class"> bggreen </xsl:attribute></xsl:for-each>
                <xsl:if test="./STATUS = 'axiom'"><xsl:attribute name="class"> bgyellow </xsl:attribute></xsl:if>
             <td>
		<a><xsl:attribute name="href"><xsl:value-of select="concat(./NAME,'/longlemmainfo.xml')"/></xsl:attribute>                  
                   <xsl:value-of select="./NAME"/>
                </a>
        </td><td>
                <xsl:if test="./STATUS = 'invalid'"><xsl:attribute name="class"> bgred </xsl:attribute></xsl:if>
                 <xsl:value-of select="./STATUS"/>
	</td><td>
                 <xsl:for-each select="./SUBSPROVED/T">all subs proved
		</xsl:for-each>
        </td><td>
		<xsl:for-each select="./USED/LE">
		<a><xsl:attribute name="href"><xsl:value-of select="concat(.,'/longlemmainfo.xml')"/></xsl:attribute> 
                <xsl:value-of select="."/></a>, 
		</xsl:for-each>
        </td></tr>
	</xsl:for-each>
	</table>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>
