<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/LEMMASUMMARY">
<html> 
<head>
<title> Theorem base summary for <xsl:value-of select="./attribute::name"/> </title>

<xsl:call-template name="styledefs"/>

</head>
<body><xsl:call-template name="unitheader"/>

	<h1> Theorem base summary for <xsl:value-of select="./attribute::name"/> </h1>

<ul>
<li> Status: <xsl:for-each select="./STATUS/T"><font color="green">Proved!</font></xsl:for-each>
             <xsl:for-each select="./STATUS/F"><font color="blue">unproved.</font></xsl:for-each>
</li>
<li> Axioms:     <xsl:value-of select="./NUMAXIOMS"/>, 
     User Theorems: <xsl:value-of select="./NUMTHEOREMS"/>
     (Proved:     <xsl:value-of select="./NUMPROVED"/>, 
     Partially proved: <xsl:value-of select="./NUMPARTIAL"/>, 
     Unproved:   <xsl:value-of select="./NUMUNPROVED"/>, 
     Invalid:    <xsl:value-of select="./NUMINVALID"/>, 
     Siginvalid: <xsl:value-of select="./NUMSIGINVALID"/>)
</li>
<li> Proof steps:  <xsl:value-of select="./NUMPROOFSTEPS"/>,
     Interactions: <xsl:value-of select="./NUMINTERACTIONS"/>
</li>
</ul>

	<table border="2" cellpadding="3">
	<xsl:for-each select="./LEMMATABLE/ROW">
	<tr>
	    <xsl:for-each select="./LEMMAINFO">
            <td>
            <xsl:choose>
	    <xsl:when test="./attribute::state = 'siginvalid'"><xsl:attribute name="class"> bgred </xsl:attribute></xsl:when>
	    <xsl:when test="./attribute::state = 'invalid'"><xsl:attribute name="class"> bgorange </xsl:attribute></xsl:when>
	    <xsl:when test="./attribute::state = 'axiom'"><xsl:attribute name="class"> bgyellow </xsl:attribute></xsl:when>
	    <xsl:when test="./attribute::state = 'unproved'"><xsl:attribute name="class"> bgblue </xsl:attribute></xsl:when>
	    <xsl:when test="./attribute::state = 'partial'"><xsl:attribute name="class"> bglightblue </xsl:attribute></xsl:when>
	    <xsl:when test="./attribute::state = 'proved'"><xsl:attribute name="class"> bggreen </xsl:attribute></xsl:when>
            <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
            <a><xsl:attribute name="href"><xsl:value-of select="concat(./LEMMANAME,'/longlemmainfo.xml')"/></xsl:attribute> 
  		<xsl:value-of select="./LEMMANAME"/>(<xsl:value-of select="./PROOFSTEPS"/>,<xsl:value-of select="./USERACTIONS"/>)</a>
            </td>
            </xsl:for-each>
        </tr>
        </xsl:for-each>
	</table>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>
