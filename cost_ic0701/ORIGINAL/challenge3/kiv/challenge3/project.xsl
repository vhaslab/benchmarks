<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/PROJECT">

<html> 
<head>
      <title> Project <xsl:value-of select="./PROJECTNAME"></xsl:value-of> </title>
      <xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="projectheader"/>

	<h1> Project <xsl:value-of select="./PROJECTNAME"></xsl:value-of> </h1>

<table>
<tr><td valign="top">
<font color="red"><h2>The units</h2></font>
<table>
<xsl:for-each select="./EXTUNITNAME">
   <tr><td>
   <xsl:value-of select="position()"/>.
   </td><td>
       <a><xsl:attribute name="href">
       <xsl:value-of select="concat(./EXTUNITPATH,'unit.xml')"/></xsl:attribute>
       <xsl:value-of select="./EXTUNITNAME"/></a>
   </td><td>
      (<xsl:value-of select="./EXTUNITTYPE"/>
   </td><td>
       <xsl:for-each select="./EXTUNITSTATUS/UNITCREATED"> created</xsl:for-each>
       <xsl:for-each select="./EXTUNITSTATUS/UNITINSTALLED"> <font color="blue">installed</font></xsl:for-each>
       <xsl:for-each select="./EXTUNITSTATUS/UNITINVALID"> <font color="red">invalid</font></xsl:for-each>
       <xsl:for-each select="./EXTUNITSTATUS/UNITPROVED"> <font color="green">installed</font></xsl:for-each>
    </td><td>
       <xsl:for-each select="./EXTUNITLIB/T"> <font color="orange">library</font></xsl:for-each>)
   </td></tr>
</xsl:for-each>
</table></td><td valign="top"><img src="devgraph.png"/></td></tr></table>
</body>
</html> 
</xsl:template>
</xsl:stylesheet>
