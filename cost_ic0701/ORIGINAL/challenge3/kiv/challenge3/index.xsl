<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/PROJECTINDEX">
<html>
<head>
<title> Project <xsl:value-of select="./attribute::name"/> </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="projectheader"/>

<h1>Project <xsl:value-of select="./attribute::name"/></h1>

<p>This is a detailed WWW representation of a KIV project with specifications, theorems,
and proofs. </p>

<p>You should begin with a look at the <a href="project.xml">project with graph</a> to get
an overview over the specifications and their structure. From there you can browse the individual
specifications and then the proofs. </p>

<p>The <a href="statistic.xml">project statistic</a> contains information about the number of
theorems, proofs, proof steps, user interactions, etc.</p>

<p>The formulas use logical symbols like and, or, always quantifier, and mathematical symbols
like empty set, union etc. To check which symbols are available in your browser you can take
a look at the <a href="kivsymbols.xml">KIV symbols</a>.</p>
</body>
</html>
</xsl:template>

</xsl:stylesheet>
