<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/THEOREMS_USE_PROPERTIES_LIST">
<html> 
<head>
<title> Theorems use properties list </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="unitheader"/>

	<h1> Theorems use properties list </h1>
	<ul>
	<xsl:for-each select="./THEOREM_USES">
	<li>
            <xsl:value-of select="./LEMMANAME"/> uses:
            <ul>
            <li> spec lemmas:
                 <xsl:for-each select="./USED_SPECLEMS/USED_SEQ">
                           <xsl:value-of select="./SPECNAME"/>/
                           <xsl:value-of select="./INSTNAME"/>/
                           <xsl:value-of select="./LEMNAME"/>, 
                 </xsl:for-each>
            </li>

            <li> simplifier rules:
                 <xsl:for-each select="./USED_SIMPRULES/USED_SEQ">
                           <xsl:value-of select="./SPECNAME"/>/
                           <xsl:value-of select="./INSTNAME"/>/
                           <xsl:value-of select="./LEMNAME"/>, 
                 </xsl:for-each>
            </li>
            </ul>
        </li>
	</xsl:for-each>
        </ul>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>

