<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/PROJECTSTATISTIC">
<html> 
<head>
<title> Project Statistics </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="projectheader"/>

	<h1> Project Statistics </h1>
        The following tables contain statistical data about the project. The most significant numbers
        that give an impression about the effort for the project are <b>Proofs - nonlib specs</b>.
        This line contains a summary for specifications that are not imported from a library, but
        unique for the project. (Library specifications are orange in the development graph, the
        other either blue or green, depending on their status.)<p />

        <b>theorems</b> contains the number of theorems (not axioms) that were invented by the user.<br />
        <b>proofs</b> shows how many proofs were done, for a completed project this is equal to
        the number of theorems.<br />
        <b>proof steps</b> shows the number of rule applications of the calculus.<br />
        <b>interactions</b> is the number rule applications that were done by the user.<p />
	<table>
	<tr>
	<!-- ************************************************************************************* -->
	<td valign="top">
	<h2> Proofs </h2>
	<table border="2" class="cent">
	<tr>
	<th class="tablehead"> unit name </th>
	<th class="tablehead"> data? </th>
	<th class="tablehead"> lib? </th>
	<th class="tablehead"> theorems </th>
	<th class="tablehead"> proofs </th>
	<th class="tablehead"> proof steps </th>
	<th class="tablehead"> interactions </th>
	</tr>
	<!-- ====================================================================================== -->
	<xsl:for-each select="./*/ONEPROOFSTAT">
	<tr>
                <xsl:for-each select="./NAME">
                <xsl:choose>
      	        <xsl:when test="local-name(./HTMLLINK) = 'HTMLLINK'">
                <td>     <a><xsl:attribute name="href">
                             <xsl:value-of select="./HTMLLINK/HTMLTARGET"/>
                            </xsl:attribute><xsl:value-of select="./HTMLLINK/HTMLSTRING"/>
                         </a>
                </td>
                <xsl:for-each select="../IS_DATA/T"><td>Yes</td></xsl:for-each>
                <xsl:for-each select="../IS_DATA/F"><td>No</td></xsl:for-each>
           
                <xsl:for-each select="../IS_LIBRARY/T"><td>Yes</td></xsl:for-each>
                <xsl:for-each select="../IS_LIBRARY/F"><td>No</td></xsl:for-each>
               </xsl:when>
               <xsl:otherwise><td class="bggreen"><xsl:value-of select="."/></td><td></td><td></td>
              </xsl:otherwise>
               </xsl:choose>
               </xsl:for-each>


            <td><xsl:value-of select="./NUMBER_OF_THEOREMS"></xsl:value-of></td>
            <td><xsl:value-of select="./NUMBER_OF_PROOFS"></xsl:value-of></td>
            <td><xsl:value-of select="./NUMBER_OF_PROOFSTEPS"></xsl:value-of></td>
            <td><xsl:value-of select="./NUMBER_OF_INTERACTIONS"></xsl:value-of></td>
        </tr>
	</xsl:for-each>
	<!-- ====================================================================================== -->
	</table>
	</td>
	<!-- ************************************************************************************* -->
	<td valign="top">
	<h2> Lines et. al. of Specifications </h2>
	<table border="2" class="cent">
	<tr>
	<th class="tablehead"> unit name </th>
	<th class="tablehead"> data? </th>
	<th class="tablehead"> lib? </th>
	<th class="tablehead"> lines </th>
	<th class="tablehead"> axioms </th>
	<th class="tablehead"> operations </th>
	<th class="tablehead"> sorts </th>
	</tr>
	<xsl:for-each select="./SPECSTATISTIC/ONELINESTAT">
	<tr>
            <td><xsl:for-each select="./NAME">
                <xsl:choose>
      	        <xsl:when test="local-name(./HTMLLINK) = 'HTMLLINK'">
                          <a><xsl:attribute name="href">
                             <xsl:value-of select="./HTMLLINK/HTMLTARGET"/>
                            </xsl:attribute><xsl:value-of select="./HTMLLINK/HTMLSTRING"/>
                         </a>
               </xsl:when>
	        <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
               </xsl:choose>
               </xsl:for-each>
           </td>
                <xsl:for-each select="./IS_DATA/T"><td>Yes</td></xsl:for-each>
                <xsl:for-each select="./IS_DATA/F"><td>No</td></xsl:for-each>
           
                <xsl:for-each select="./IS_LIBRARY/T"><td>Yes</td></xsl:for-each>
                <xsl:for-each select="./IS_LIBRARY/F"><td>No</td></xsl:for-each>
           
            <td><xsl:value-of select="./LINES_OF_SPEC"></xsl:value-of></td>
            <td><xsl:value-of select="./NUMBER_OF_AXIOMS"></xsl:value-of></td>
            <td><xsl:value-of select="./NUMBER_OF_OPERATIONS"></xsl:value-of></td>
            <td><xsl:value-of select="./NUMBER_OF_SORTS"></xsl:value-of></td>
        </tr>
	</xsl:for-each>
	</table>
	</td>
	<!-- ************************************************************************************* -->
	</tr>
	</table>
	</body>
	</html> 
</xsl:template>
</xsl:stylesheet>

