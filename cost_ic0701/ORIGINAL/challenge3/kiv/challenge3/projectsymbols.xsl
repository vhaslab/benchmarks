<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                 xmlns="http://www.w3.org/1999/xhtml"> 

<xsl:output encoding="UTF-8"/>
<xsl:include href="common.xsl"/>

<xsl:template match="/PROJECTSYMBOLS">
<html> 
<head>
<title> All project symbols </title>
<xsl:call-template name="styledefs"/>
</head>
<body><xsl:call-template name="projectheader"/>

<h1> All project symbols </h1>

<table border="1" class="cent">
	<tr>
	<th class="tablehead"> symbol </th>
	<th class="tablehead"> specification </th>
	<th class="tablehead"> entry </th>
	</tr>
	<xsl:for-each select="./SYMENTRY">
        <tr><td><xsl:value-of select="./SYMBOL"/></td>
            <td>
            <xsl:for-each select="./SPECNAME">
                          <a><xsl:attribute name="href">
                             <xsl:value-of select="./HTMLLINK/HTMLTARGET"/>
                            </xsl:attribute><xsl:value-of select="./HTMLLINK/HTMLSTRING"/>
                         </a>
           </xsl:for-each></td><td>
            <xsl:value-of select="./TYPE"/>,
            <xsl:value-of select="./SIGENTRY"/></td>
        </tr>
	</xsl:for-each>
</table>
</body>
</html> 
</xsl:template>
</xsl:stylesheet>

