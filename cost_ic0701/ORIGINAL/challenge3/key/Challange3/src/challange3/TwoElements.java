/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package challange3;

/**
 *
 * @author christoph
 */
public class TwoElements {

    //@ requires result.length == 2;
    //@ requires array != result;
    //@ requires array.length >= 4;
    //@ requires (\forall int i; 0 <= i && i < array.length; 0 <= array[i] && array[i] < array.length - 2);
    //@ requires (\exists int x; x>=0 && x<array.length - 2; (\exists int y; y>=0 && y < array.length -2 && x != y;  2 == (\sum int i; 0 <= i && i < array.length; array[i] == x ? 1 : 0) && 2 == (\sum int i; 0 <= i && i < array.length; array[i] == y ? 1 : 0)));
    //@ ensures 2 == (\sum int i; 0<=i && i < array.length; array[i] == result[0] ? 1 : 0);
    //@ ensures 2 == (\sum int i; 0<=i && i< array.length; array[i] == result[1] ? 1 : 0);
    //@ ensures result[0] != result[1];
    //@ assignable result[*];
    static void findTwo(int[] array, int[] result) {
        int currResult = 0;

        /*@
            assignable result[*];
            loop_invariant i>=0 && i <= array.length - 2 && (currResult == 0 || currResult == 1 || currResult == 2) &&
            (currResult == 1 ==> 2 == (\sum int k; 0<=k && k<array.length; array[k] == result[0] ? 1 : 0)) &&  
            (currResult == 2 ==> (2 == (\sum int k; 0<=k && k<array.length; array[k] == result[0] ? 1 : 0) && 2 == (\sum int k; 0<=k && k<array.length; array[k] == result[1] ? 1 : 0)))
            ;
            decreases 2 - currResult ;
         @*/
        for(int i=0 ; i < array.length - 2; i++) {
            int count = countAcc(array, i);
            if(count == 2) {
              result[currResult] = i;
              currResult++;
              if(currResult == 2) 
                break;
            }
        }
    }
    
    //@ ensures \result == (\sum int i; 0 <= i && i < array.length; array[i] == value ? 1 : 0);
    //@ assignable \nothing;
   static int countAcc(int[] array, int value) {
            int count = 0;
            /*@
            assignable \nothing;
            loop_invariant j>=0 && j<=array.length && count == (\sum int i; 0<=i && i<j; array[i] == value ? 1 : 0);
            decreases array.length - j;
            @*/
            for(int j=0 ; j < array.length; j++) {
                if(array[j] == value) {
                    count++;
                }
            }
            return count;
   }

}
