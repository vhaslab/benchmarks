/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package challange3;

/**
 *
 * @author christoph
 */
public class TwoElements {
    
    /*@ normal_behavior
         requires    4 <= array.length;
         requires    rresult.length == 2;
         requires    array != rresult;
         requires    smin + 1 < smax;
         requires    (\exists int x; x >= smin && x < smax; (\exists int y; x != y && y >= smin && y < smax;
                         (\sum int i; 0 <= i && i < array.length; array[i] == x ? 1 : 0) >= 2 && (\sum int i; 0 <= i && i < array.length; array[i] == y ? 1 : 0) >= 2));
         ensures     rresult[0] != rresult[1];
         ensures     (\sum int i; 0 <= i && i < array.length; array[i] == rresult[0] ? 1 : 0) >= 2;
         ensures     (\sum int i; 0 <= i && i < array.length; array[i] == rresult[1] ? 1 : 0) >= 2;
         assignable  rresult[*];
      @*/
    static void findTwo(int[] array, int[] rresult, int smin, int smax) {
        int currResult = 0;
        int startFrom = smin;
        /*@ 
            loop_invariant startFrom >= smin && startFrom <= smax && 0 <= currResult && currResult <= 2 &&
               (currResult == 0 ==> (startFrom < smax && (\exists int x; x >= startFrom && x < smax;
                   (\exists int y; y >= startFrom && y < smax && x != y;
                      (\sum int i; 0 <= i && i < array.length; array[i] == x ? 1 : 0) >= 2 && 
                      (\sum int i; 0 <= i && i < array.length; array[i] == y ? 1 : 0) >= 2)))) &&
               (currResult == 1 ==> (startFrom < smax && rresult[0] < startFrom && (\exists int y; y >= startFrom && y < smax; 
                      (\sum int i; 0 <= i && i < array.length; array[i] == y ? 1 : 0) >= 2) &&
                      (\sum int i; 0 <= i && i < array.length; array[i] == rresult[0] ? 1 : 0) >= 2)) &&
               (currResult == 2 ==> (rresult[0] < startFrom && rresult[1] < startFrom && rresult[0] != rresult[1] &&
                    (\sum int i; 0 <= i && i < array.length; array[i] == rresult[0] ? 1 : 0) >= 2 && 
                    (\sum int i; 0 <= i && i < array.length; array[i] == rresult[1] ? 1 : 0) >= 2));           
            assignable rresult[*];
            decreases smax - startFrom;
         @*/
         while(currResult < 2) {
            if(countAcc(array, startFrom) >= 2) {
                rresult[currResult] = startFrom;
                currResult++;
            }
            startFrom++;
            if(startFrom == smax) break;
         }
    }

    /*@ normal_behavior
            ensures     \result ==
                        (\sum int i; 0 <= i && i < array.length; array[i] == value ? 1 : 0);
      @*/
    static int /*@ pure @*/ countAcc(int[] array, int value) {
        int count = 0;
        /*@ loop_invariant
                0 <= j && j <= array.length &&
                count == (\sum int i; 0 <= i && i < j; array[i] == value ? 1 : 0);
            assignable  \nothing;
            decreases   array.length - j;
          @*/
        for(int j=0 ; j < array.length; j++) {
            if(array[j] == value) {
                count++;
            }
        }
        return count;
    }

}
