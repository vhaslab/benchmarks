/* Example where DAGGER is exponentially better thab SLAM, BLAST, SATABS */
int nondet(void);
int main () {
int x=0;

if (nondet()) x = x+1;
else x = x+22; 

if (nondet()) x = x+1;
else x = x+20; 

if (nondet()) x = x+1;
else x = x+18; 

if (nondet()) x = x+1;
else x = x+16; 

if (nondet()) x = x+1;
else x = x+14; 

if (nondet()) x = x+1;
else x = x+12; 

if (nondet()) x = x+1;
else x = x+10; 

if (nondet()) x = x+1;
else x = x+8; 

if (nondet()) x = x+1;
else x = x+6; 

if (nondet()) x = x+1;
else x = x+4; 

if (nondet()) x = x+1;
else x = x+2; 

assert (x <= 132);

return 0;
}
