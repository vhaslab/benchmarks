
int nondet () { int i; return i;}

void mime7to8()
{ register char *p ;
  char *fbufp ;
  char canary[10] ;
  char fbuf[51] ;
  int c1 ;
  int c2 ;
  int c3 ;
  int c4 ;
  unsigned short const **tmp ;
  unsigned short const **tmp___0 ;
  unsigned short const **tmp___1 ;
  unsigned short const **tmp___2 ;
  char *tmp___3 ;
  char *tmp___4 ;
  char *tmp___5 ;
  int tmp___6 ;
  int id;
  {
  p = nondet();
  if ((unsigned long )p == (unsigned long )((void *)0)) {
    return;
  }
  tmp___6 = nondet();
  if (tmp___6 == 0) {
    fbufp = fbuf; id=0;
    while (1) {
      c1 = nondet();
      if (! (c1 != -1)) {
        break;
      }
      if ((c1 & -128) == 0) {
        tmp = nondet();
        if ((int const )*(*tmp + c1) & 8192) {
          continue;
        }
      }
      while (1) {
        c2 = nondet();
        if ((c2 & -128) == 0) {
          tmp___0 = nondet();
          if (! ((int const )*(*tmp___0 + c2) & 8192)) {
            break;
          }
        } else {
          break;
        }
      }
      if (c2 == -1) {
        break;
      }
      while (1) {
        c3 = nondet();
        if ((c3 & -128) == 0) {
          tmp___1 = nondet();
          if (! ((int const )*(*tmp___1 + c3) & 8192)) {
            break;
          }
        } else {
          break;
        }
      }
      if (c3 == -1) {
        break;
      }
      while (1) {
        c4 = nondet();
        if ((c4 & -128) == 0) {
          tmp___2 = nondet();
          if (! ((int const )*(*tmp___2 + c4) & 8192)) {
            break;
          }
        } else {
          break;
        }
      }
      if (c4 == -1) {
        break;
      }
      if (c1 == 61) {
        continue;
      } else {
        if (c2 == 61) {
          continue;
        }
      }
      if (c1 < 0) {
        c1 = -1;
      } else {
        if (c1 > 127) {
          c1 = -1;
        } else {
          c1 = nondet();
        }
      }
      if (c2 < 0) {
        c2 = -1;
      } else {
        if (c2 > 127) {
          c2 = -1;
        } else {
          c2 = nondet();
        }
      }
      assert (id >= 0); assert (id <= 50);
      *fbufp = (unsigned char )((c1 << 2) | ((c2 & 48) >> 4));
      tmp___3 = fbufp;
      fbufp ++; id++;
      if ((int )*tmp___3 == 10) {
          fbufp --; id--;
      	  assert (id >= 0); assert (id <= 50);
          if ((int )*fbufp != 10) {
            fbufp ++; id++;
          } else {
            fbufp --; id--;
      	    assert (id >= 0); assert (id <= 50);
            if ((int )*fbufp != 13) {
              fbufp ++; id++;
            }
          }
      	  assert (id >= 0); assert (id <= 50);
          *fbufp = (unsigned char )'\000';
          fbufp = fbuf; id=0;
      }
      if (c3 == 61) {
        continue;
      }
      if (c3 < 0) {
        c3 = -1;
      } else {
        if (c3 > 127) {
          c3 = -1;
        } else {
          c3 = nondet();
        }
      }
      assert (id >= 0); assert (id <= 50);
      *fbufp = (unsigned char )(((c2 & 15) << 4) | ((c3 & 60) >> 2));
      tmp___4 = fbufp;
      fbufp ++; id++;
      if ((int )*tmp___4 == 10) {
          fbufp --; id--;
      	  assert (id >= 0); assert (id <= 50);
          if ((int )*fbufp != 10) {
            fbufp ++; id++;
          } else {
            fbufp --; id--;
      	    assert (id >= 0); assert (id <= 50);
            if ((int )*fbufp != 13) {
              fbufp ++; id++;
            }
          }
      	  assert (id >= 0); assert (id <= 50);
          *fbufp = (unsigned char )'\000';
          //printf("resetting fbufp\n");
          fbufp = fbuf; id=0;
      }
      if (c4 == 61) {
        continue;
      }
      if (c4 < 0) {
        c4 = -1;
      } else {
        if (c4 > 127) {
          c4 = -1;
        } else {
          c4 = nondet();
        }
      }
      assert (id >= 0); assert (id <= 50);
      *fbufp = (unsigned char )(((c3 & 3) << 6) | c4);
      tmp___5 = fbufp;
      fbufp ++; id++;
      if ((int )*tmp___5 == 10) {
          fbufp --; id--;
      	  assert (id >= 0); assert (id <= 50);
          if ((int )*fbufp != 10) {
            fbufp ++; id++;
          } else {
            fbufp --; id--;
      	    assert (id >= 0); assert (id <= 50);
            if ((int )*fbufp != 13) {
              fbufp ++; id++;
            }
          }
      	  assert (id >= 0); assert (id <= 50);
          *fbufp = (unsigned char )'\000';
          //printf("resetting fbufp\n");
          fbufp = fbuf; id=0;
      }
    }
    if (id > 0) {
      assert (id >= 0); assert (id <= 50);
      *fbufp = (unsigned char )'\000';
    }
  } else {
    //printf("We only support base64 encoding...\n");
  }
  return;
}
}

