#!/bin/bash

for i in `find . -name "*.c"`
do
  echo "Compiling $i"
  clang $i -w -O0 -c -emit-llvm -I include -o `dirname $i`/`basename $i .c`.bc
done
