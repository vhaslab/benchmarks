#!/bin/bash
cbmc ex11.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --unwind 1000 --no-unwinding-assertions
