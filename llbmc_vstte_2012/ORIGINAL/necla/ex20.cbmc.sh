#!/bin/bash
cbmc ex20.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --unwind 1025
