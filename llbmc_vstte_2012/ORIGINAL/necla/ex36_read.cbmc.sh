#!/bin/bash
cbmc ex36.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --function __llbmc_main --no-unwinding-assertions
