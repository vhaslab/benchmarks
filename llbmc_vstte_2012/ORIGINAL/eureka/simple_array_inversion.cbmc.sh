#!/bin/bash
cbmc simple_array_inversion.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --unwind 3
