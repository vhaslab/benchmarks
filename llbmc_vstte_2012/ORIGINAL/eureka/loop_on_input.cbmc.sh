#!/bin/bash
cbmc loop_on_input.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --unwind 4
