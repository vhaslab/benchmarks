#!/bin/bash
cbmc complex_guard.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --unwind 2
