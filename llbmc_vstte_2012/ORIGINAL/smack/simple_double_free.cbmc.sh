#!/bin/bash
cbmc simple_double_free.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check
