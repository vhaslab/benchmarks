#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define __llbmc_assert(b) __ESBMC_assert(b, __FILE__)
#define __llbmc_assume(b) __ESBMC_assume(b)

#define __llbmc_nondef_char() nondet_char()
#define __llbmc_nondef_int() nondet_int()
#define __llbmc_nondef_uint16_t() nondet_uint16_t()
#define __llbmc_nondef_uint32_t() nondet_uint32_t()

char nondet_char();
int nondet_int();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();

#ifdef __cplusplus
}
#endif

