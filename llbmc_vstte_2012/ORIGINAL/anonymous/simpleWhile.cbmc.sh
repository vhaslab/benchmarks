#!/bin/bash
cbmc simpleWhile.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --function simpleWhile --unwind 1001
