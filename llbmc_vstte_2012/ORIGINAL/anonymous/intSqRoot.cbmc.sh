#!/bin/bash
cbmc intSqRoot.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --function intSqRoot --unwind 200
