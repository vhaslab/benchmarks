#!/bin/bash
cbmc fibcall.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check --unwind 30
