#!/bin/bash
cbmc matmul.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check
