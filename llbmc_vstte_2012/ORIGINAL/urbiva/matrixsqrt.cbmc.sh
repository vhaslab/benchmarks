#!/bin/bash
cbmc matrixsqrt.c -I ../include/cbmc --bounds-check --div-by-zero-check --pointer-check --overflow-check
